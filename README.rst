=====================
ansible-usb-turntable
=====================

https://gitlab.com/jfriis/ansible-usb-turntable

Configure USB Turntable with icecast and darkice, ala https://github.com/basdp/USB-Turntables-to-Sonos-with-RPi

Use systemd instead of init.d ala https://www.jonadams.com/wireless/?p=1026


Usage
=====

.. code::

   ansible-playbook playbook.yml


Vars
====

.. code:: yaml

   hostname: localhost                                                                                                                          
   port: 8000                                                                                                                                   
   mountpoint: turntable.mp3                                                                                                                    
   admin_user: admin                                                                                                                            
   admin_password: 1234                                                                                                                         
   source_password: 1234                                                                                                                        
   relay_password: 1234
